{def $classes = ezini('ListSettings', 'IncludeClasses', 'stats.ini' )
     $page_uri = '/ngstats/stats'
     $user_filter = array()
     $limit = 25
     $begindate = ''
     $enddate = ''
     $date_filter = array()}

{def $params = module_params().parameters}

{if and(is_set($params.class), ne($params.class, ''))}
  {set $classes = array( $params.class )}
{/if}
{if is_set($params.class)}
  {set $page_uri = concat($page_uri, '/', $params.class)}
{/if}
{if and(is_set($params.user), ne($params.user, ''))}
  {set $user_filter = array( 'owner', '=', $params.user )}
{/if}

{if is_set($params.user)}
  {set $page_uri = concat($page_uri, '/', $params.user)}
{/if}

{if is_set($params.begindate)}
  {set $begindate = $params.begindate}
  {set $page_uri = concat($page_uri, '/', $params.begindate)}
{/if}

{if is_set($params.enddate)}
  {set $enddate = $params.enddate}
  {set $page_uri = concat($page_uri, '/', $params.enddate)}
{/if}

{if and( ne($begindate, ''), ne($enddate, '') )}
  {set $date_filter = array( 'published', 'between', array($begindate, $enddate) )}
{/if}

{def $attribute_filter = array()}
{if and(ne($user_filter|count(), 0), ne($date_filter|count(), 0))}
  {set $attribute_filter = $attribute_filter|append('and', $user_filter, $date_filter)}
{elseif and(eq($user_filter|count(), 0), ne($date_filter|count(), 0))}
  {set $attribute_filter = $attribute_filter|append( $date_filter )}
{elseif and(ne($user_filter|count(), 0), eq($date_filter|count(), 0))}
  {set $attribute_filter = $attribute_filter|append( $date_filter )}
{/if}



{def $object_count_hash = array()}
{def $object_fetch_hash = array()}

{if eq($attribute_filter|count(), 0)}
  {def $object_count_hash = hash( 'parent_node_id', 1,
                                  'main_node_only', true(),
                                  'class_filter_type', include,
                                  'class_filter_array', $classes )
       $object_fetch_hash = hash( 'parent_node_id', 1,
                                  'sort_by', array('published', true()),
                                  'main_node_only', true(),
                                  'class_filter_type', 'include',
                                  'class_filter_array', $classes,
                                  'limit', $limit,
                                  'offset', $view_parameters.offset )}
{else}
   {def $object_count_hash = hash( 'parent_node_id', 1,
                                   'main_node_only', true(),
                                   'class_filter_type', include,
                                   'class_filter_array', $classes,
                                   'attribute_filter', $attribute_filter )
        $object_fetch_hash = hash( 'parent_node_id', 1,
                                   'sort_by', array('published', true()),
                                   'main_node_only', true(),
                                   'class_filter_type', 'include',
                                   'class_filter_array', $classes,
                                   'attribute_filter', $attribute_filter,
                                   'limit', $limit,
                                   'offset', $view_parameters.offset )}
{/if}

{def $object_count = fetch( 'content', 'tree_count', $object_count_hash )}
{def $object_fetch = fetch( 'content', 'tree', $object_fetch_hash )}


<div class="context-block">

    <div class="box-header"><div class="box-tc"><div class="box-ml"><div class="box-mr"><div class="box-tl"><div class="box-tr">
        <h2 class="context-title">{$object_count} objects</h2>
    </div></div></div></div></div></div>

</div>

<div class="box-ml"><div class="box-mr"><div class="box-content">

  <div class="content-navigation-childlist">
  <table class="list" cellspacing="0">
  <tbody>
  <tr>
      <th class="name">{'Name'|i18n( 'design/admin/node/view/full' )}</th>
      <th class="class">{'Type'|i18n( 'design/admin/node/view/full' )}</th>
      <th class="class">{'Creator'|i18n( 'design/admin/node/view/full' )}</th>
      <th class="class">Broj znakova</th>
      <th class="modified">{'Published'|i18n( 'design/admin/node/view/full' )}</th>
      <th class="edit">&nbsp;</th>
  </tr>
  {foreach $object_fetch as $k => $node sequence array( 'bgdark', 'bglight' ) as $style}
  <tr class="{$style}">
  <td>
      <a href={$node.url_alias|ezurl()}>{$node.name|wash()}</a>
  </td>
  <td class="class">
      <a href={concat( 'classlists/list/', $node.class_identifier )|ezurl()}>{$node.class_name|wash()}</a>
  </td>
  <td class="class">
      {$node.object.owner.name}
  </td>
  <td class="class">
    {if or($classes|contains($node.object.content_class.identifier), $classes|contains($node.object.content_class.id))}
      {def $attributes_to_check = ezini( concat('Count_Chars_', $node.object.content_class.identifier), 'Attribute', 'stats.ini' )}
      {def $sign_count = 0}
      {def $stripped_attribute = ''}
      {def $stripped_count = 0}
      {foreach $node.data_map as $attribute}
        {foreach $attributes_to_check as $checkpoint}
          {if eq($attribute.contentclass_attribute_identifier, $checkpoint)}
            {set $stripped_attribute = strip_tags($attribute.data_text)}
            {set $stripped_count = $stripped_attribute|count_chars()}
            {set $sign_count = $sign_count|sum($stripped_count)}
          {/if}
        {/foreach}
      {/foreach}
      {$sign_count}
    {/if}
  </td>
  <td class="modified">
      {$node.object.published|datetime('custom', '%d. %m. %Y')}
  </td>
  <td class="edit">
      <a href={concat( '/content/edit/', $node.object.id )|ezurl()}><img src={'edit.gif'|ezimage()} alt="{'Modify'|i18n( 'classlists/list')}" /></a>
  </td>
  </tr>
  {/foreach}

  </tbody>
  </table>
  </div>

  <div class="context-toolbar">
  {include name=navigator uri='design:navigator/google.tpl'
                          page_uri=$page_uri
                          item_count=$object_count
                          view_parameters=$view_parameters
                          item_limit=$limit}
          </div>

</div></div></div>
