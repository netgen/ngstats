<div class="box-header">
<div class="box-tc"><div class="box-ml"><div class="box-mr"><div class="box-tl"><div class="box-tr">
	<h4>Options</h4>
</div></div></div></div></div>
</div>

<div class="box-bc"><div class="box-ml"><div class="box-mr"><div class="box-bl"><div class="box-br">
<div class="box-content">
  {def $params = module_params().parameters}
	<form action="/administracija/ngstats/stats" method="post" >
		<label for="user_class">Class:</label>
		<select id="user_class" name="user_class">
		  <option value="">Choose a class</option>
			{foreach $classlist as $class_obj}
				<option value="{$class_obj.id}" {if eq($class_obj.id, $params.class)}selected{/if} >{$class_obj.name}</option>
			{/foreach}				
		</select>
		<label for="user_id">User:</label>
		<select id="user_id" name="user_id">
		  <option value="">Choose a User</option>
			{foreach $userlist as $user_obj}
				<option value="{$user_obj.contentobject.id}" {if eq($user_obj.contentobject.id, $params.user)}selected{/if} >{$user_obj.contentobject.name}</option>
			{/foreach}				
		</select>
		<label for="begindate">Begin date:</label>
		{if is_set($params.begindate)}
		  {def $begindate = $params.begindate|datetime('custom', '%d.%n.%Y')}
		  {def $begindate_exploded = $begindate|explode('.')}
		  {def $bd = $begindate_exploded.0}
		  {def $bm = $begindate_exploded.1}
		  {def $by = $begindate_exploded.2}
    {/if}
		<div id="begindate">
		  <input type="text" name="bd" id="bd" {if is_set($bd)}value="{$bd}"{/if} style="width:17px;" />
		  <select name="bm" id="bm" />
		    <option value="" >Choose month</option>
		    <option value="1" {if and(is_set($bm), eq($bm, 1))}selected{/if} >January</option>
		    <option value="2" {if and(is_set($bm), eq($bm, 2))}selected{/if} >February</option>
		    <option value="3" {if and(is_set($bm), eq($bm, 3))}selected{/if} >March</option>
		    <option value="4" {if and(is_set($bm), eq($bm, 4))}selected{/if} >April</option>
		    <option value="5" {if and(is_set($bm), eq($bm, 5))}selected{/if} >May</option>
		    <option value="6" {if and(is_set($bm), eq($bm, 6))}selected{/if} >June</option>
		    <option value="7" {if and(is_set($bm), eq($bm, 7))}selected{/if} >July</option>
		    <option value="8" {if and(is_set($bm), eq($bm, 8))}selected{/if} >August</option>
		    <option value="9" {if and(is_set($bm), eq($bm, 9))}selected{/if} >September</option>
		    <option value="10" {if and(is_set($bm), eq($bm, 10))}selected{/if} >October</option>
		    <option value="11" {if and(is_set($bm), eq($bm, 11))}selected{/if} >November</option>
		    <option value="12" {if and(is_set($bm), eq($bm, 12))}selected{/if} >December</option>
		  </select>
		  <input type="text" name="by" id="by" {if is_set($by)}value="{$by}"{/if} style="width:30px;" />
		</div>
		<br />
		<label for="enddate">End date:</label>
		{if is_set($params.enddate)}
		  {def $enddate = $params.enddate|datetime('custom', '%d.%n.%Y')}
		  {def $enddate_exploded = $enddate|explode('.')}
		  {def $ed = $enddate_exploded.0}
		  {def $em = $enddate_exploded.1}
		  {def $ey = $enddate_exploded.2}
    {/if}
		<div id="enddate">
		  <input type="text" name="ed" id="ed" {if is_set($ed)}value="{$ed}"{/if} style="width:17px;" />
		  <select name="em" id="em" />
		    <option value="" >Choose month</option>
		    <option value="1" {if and(is_set($em), eq($em, 1))}selected{/if} >January</option>
		    <option value="2" {if and(is_set($em), eq($em, 2))}selected{/if} >February</option>
		    <option value="3" {if and(is_set($em), eq($em, 3))}selected{/if} >March</option>
		    <option value="4" {if and(is_set($em), eq($em, 4))}selected{/if} >April</option>
		    <option value="5" {if and(is_set($em), eq($em, 5))}selected{/if} >May</option>
		    <option value="6" {if and(is_set($em), eq($em, 6))}selected{/if} >June</option>
		    <option value="7" {if and(is_set($em), eq($em, 7))}selected{/if} >July</option>
		    <option value="8" {if and(is_set($em), eq($em, 8))}selected{/if} >August</option>
		    <option value="9" {if and(is_set($em), eq($em, 9))}selected{/if} >September</option>
		    <option value="10" {if and(is_set($em), eq($em, 10))}selected{/if} >October</option>
		    <option value="11" {if and(is_set($em), eq($em, 11))}selected{/if} >November</option>
		    <option value="12" {if and(is_set($em), eq($em, 12))}selected{/if} >December</option>
		  </select>
		  <input type="text" name="ey" id="ey" {if is_set($ey)}value="{$ey}"{/if} style="width:30px;" />
		</div>
		<br />
		<input type="submit" class="button" value="Go" />
	</form>
		
</div>
</div></div></div></div></div>
