<?php /*
#
# $Id: menu.ini.append.php 14 2009-11-11 21:39:38Z dpobel $
# $HeadURL: http://svn.projects.ez.no/ezclasslists/trunk/ezclasslists/settings/menu.ini.append.php $
#

[NavigationPart]
Part[ngstats]=Statistics

[TopAdminMenu]
Tabs[]=ngstats

[Topmenu_ngstats]
NavigationPartIdentifier=ngstats
Name=Statistics
Tooltip=Statistics of user activity
URL[]
URL[default]=ngstats/stats
Enabled[]
Enabled[default]=true
Enabled[browse]=false
Enabled[edit]=false
Shown[]
Shown[navigation]=true
Shown[default]=true
Shown[browse]=true

*/ ?>
