<?php /*

[ListSettings]
# Fill this array with class identifiers to have only
# those classes listed in the left menu
IncludeClasses[]
IncludeClasses[]=vidi_article
IncludeClasses[]=vidi_category
IncludeClasses[]=vidiauto_lexicon_note
IncludeClasses[]=vidiauto_history_note
IncludeClasses[]=vidiauto_vehicle
IncludeClasses[]=vidiauto_vehicle_priceitem
# Fill this array with usergroup names to have only
# those users listed in the left menu
IncludeUserGroups[]
IncludeUserGroups[]=Partners
IncludeUserGroups[]=VIDIAUTO urednici
IncludeUserGroups[]=Administrator users

# For each of the classes determine which
# attributes to use during the character
# count

[Count_Chars_vidi_article]
Attribute[]
Attribute[]=intro
Attribute[]=short_intro
Attribute[]=body

[Count_Chars_vidi_theme]
Attribute[]
Attribute[]=description

[Count_Chars_vidi_category]
Attribute[]
Attribute[]=short_description
Attribute[]=description

[Count_Chars_vidiauto_lexicon_note]
Attribute[]
Attribute[]=short_body

[Count_Chars_vidiauto_history_note]
Attribute[]
Attribute[]=short_body

[Count_Chars_vidiauto_vehicle]
Attribute[]
Attribute[]=intro
Attribute[]=short_intro
Attribute[]=body
Attribute[]=vehicle_additional_tech_details

[Count_Chars_vidiauto_vehicle_priceitem]
Attribute[]
Attribute[]=vehicle_details

*/ ?>
