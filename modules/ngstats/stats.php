<?php
// SOFTWARE NAME: 
// SOFTWARE RELEASE: 1.1
// COPYRIGHT NOTICE: 
// SOFTWARE LICENSE: GNU General Public License v2.0
// NOTICE: >
//   This program is free software; you can redistribute it and/or
//   modify it under the terms of version 2.0  of the GNU General
//   Public License as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of version 2.0 of the GNU General
//   Public License along with this program; if not, write to the Free
//   Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//   MA 02110-1301, USA.

$http = eZHTTPTool::instance();
$hasPost = false;
$tpl = eZTemplate::factory();

$Module = $Params["Module"];

// ovo je primjer kako izvuci class parametar
//$Params['class'];
//$Params['user'];

$Result = array();

$stats_ini = eZINI::instance('stats.ini');

$ini_classes = $stats_ini->variable('ListSettings', 'IncludeClasses');
$ini_usergroups = $stats_ini->variable('ListSettings', 'IncludeUserGroups');

$classObjects = eZPersistentObject::fetchObjectList( eZContentClass::definition(),
											  		 null);
$classList = array();
foreach ($classObjects as $object)
{
	if (in_array($object->attribute('identifier'), $ini_classes))
	{
		$classList[]=$object;
	}
}

$userObjects = eZPersistentObject::fetchOBjectList( eZUser::definition(),
													null );
$userList = array();

foreach ($userObjects as $user)
{
	// $userGroup = $user->attribute( 'contentobject' )->attribute( 'main_node' )->attribute( 'parent' )->attribute( 'name' );
	$userGroups = $user->groups( true );
	foreach($userGroups as $userGroupObject)
	{
		if (in_array($userGroupObject->attribute('name'), $ini_usergroups))
		{
			$userList[]=$user;
			break;
		}
	}
}

//setting the module param class
$classIdentifier = '';
if ( isset( $Params['class'] ) )
{
    $classIdentifier = $Params['class'];
}
if ( $http->hasPostVariable( 'user_class' ) )
{
    $classIdentifier = $http->postVariable( 'user_class' );
    $hasPost = true;
}

//setting the module param user
$userIdentifier = '';
if ( isset( $Params['user'] ) )
{
    $userIdentifier = $Params['user'];
}
if ( $http->hasPostVariable( 'user_id' ) )
{
    $userIdentifier = $http->postVariable( 'user_id' );
    $hasPost = true;
}

//setting the module param begindate
$begindate = mktime(0, 0, 0, 1, 1, 1972);
$bd = 1;
$bm = 1;
$by = 1972;

if ( isset( $Params['begindate'] ) )
{
    $begindate = $Params['begindate'];
}

if ( $http->hasPostVariable( 'bd' ) && ($http->postVariable( 'bd' )!='') )
{
    $bd = $http->postVariable( 'bd' );
    $hasPost = true;  
}

if ( $http->hasPostVariable( 'bm' ) && ($http->postVariable( 'bm' )!='') )
{
    $bm = $http->postVariable( 'bm' );
    $hasPost = true;  
}

if ( $http->hasPostVariable( 'by' ) && ($http->postVariable( 'by' )!='') )
{
    $by = $http->postVariable( 'by' );
    $hasPost = true;  
}

$begindate = mktime(0, 0, 0, $bm, $bd, $by);

//setting the module param enddate
$enddate = time();
$ed = 0;
$em = 0;
$ey = 0;

if ( isset( $Params['enddate'] ) )
{
    $enddate = $Params['enddate'];
}

if ( $http->hasPostVariable( 'ed' ) && ($http->postVariable( 'ed' )!='') )
{
    $ed = $http->postVariable( 'ed' );
    $hasPost = true;  
}

if ( $http->hasPostVariable( 'em' ) && ($http->postVariable( 'em' )!='') )
{
    $em = $http->postVariable( 'em' );
    $hasPost = true;  
}

if ( $http->hasPostVariable( 'ey' ) && ($http->postVariable( 'ey' )!='') )
{
    $ey = $http->postVariable( 'ey' );
    $hasPost = true;  
}

if ( ($ed!=0) && ($em!=0) && ($ey!=0) ) {
  $enddate = mktime(23, 59, 59, $em, $ed, $ey);  
}

$tpl->setVariable('classlist', $classList);
$tpl->setVariable('userlist', $userList);
$offset = $Params['Offset'];
$tpl->setVariable( 'view_parameters', array( 'offset' => $offset ) );

if ($hasPost) 
{
    // converting post variables into ordered parameters
    $Module->redirectToView( 'stats', array( $classIdentifier,
                                            $userIdentifier,
                                            $begindate,
                                            $enddate ) );
} 

$Result['content'] = $tpl->fetch('design:ngstats/stats.tpl');
$Result['left_menu'] = "design:ngstats/leftmenu.tpl";
$Result['path'] = array( array( 'url' => 'ngstats/stats',
                                'text' => 'User statistics' ) );

?>
